    import java.util.*;

    public class Meli {

      public static void main(String[] args) {
          System.out.println(counts(Arrays.asList(1,2,3),Arrays.asList(2,4)));
          System.out.println(counts(Arrays.asList(1,4,2,4),Arrays.asList(3,5)));
          System.out.println(counts(Arrays.asList(2,10,5,4,8),Arrays.asList(3,1,7,8)));
      }
  
      public static List<Integer> counts(List<Integer> teamA, List<Integer> teamB) {
          List<Integer> scoreResume = new ArrayList<>(teamB.size());
          for(Integer goalsB : teamB){
              int matches = 0;
              for(Integer goalsA : teamA) {
                  if (goalsA <= goalsB) {
                      matches++;
                  }
              }
              scoreResume.add(matches);
          }
          return scoreResume;
      }

    }
